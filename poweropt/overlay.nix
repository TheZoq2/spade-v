(pkgs: super: {
  # Specify the sub-architecture for the RISC-V gcc
  pkgsCross = super.pkgsCross // {
    # Our platform requires to set the gcc.arch
    riscv32-embedded = import super.path (super.config // {
      crossSystem = {
        config = "riscv32-none-elf";
        libc = "newlib";
        gcc.arch = "rv32i";
      };
    });
  };
})
