let
  # Pinned things with Niv
  sources = (import ./nix/sources.nix);

  # Pinned nixpkgs with overlays applied
  pkgs = import sources.pkgs {
    overlays = [
      (import ./overlay.nix)
      (import sources.moz-overlay)
    ];
  };
in rec {
  inherit sources pkgs;

  packages = pkgs: let
  in with pkgs; [
    # Compilers
    pkgs.pkgsCross.riscv32-embedded.stdenv.cc # Cross-GCC for riscv32-none-elf; rv32i
    python3
  ];
}
