use std::ports::new_mut_wire;
use std::ports::read_mut_wire;

use spadev::cpu::ProgramLoadSignals;
use spadev::cpu::cpu;
use spadev::cpu::CpuResult;
use spadev::bus::ControlSignals;
use spadev::bus::Bus;
use spadev::bus::Buses;
use spadev::bus::Command;
use spadev::bus::nop_control;
use spadev::program_load_adapter::program_load_adapter;
use spadev::peripherals::led::led_driver;
use spadev::peripherals::timer::timer;
use spadev::memory::data_memory;

struct Out {
    trace: spadev::cpu::Trace,
    insn_control: ControlSignals,
    data_control: ControlSignals,
    data_is_write: bool,
    data_write_value: uint<32>,
    pc: uint<32>,
}

#[no_mangle]
entity top(
    clk: clock,
    rst: bool,

    insn_data: uint<32>,
    data_data: uint<32>,
) -> Out {
    let insn_control = inst new_mut_wire();
    let data_control = inst new_mut_wire();

    let PROGRAM_BASE = 0x8000_0000;

    let buses = Buses$(
        data: Bus$(control: data_control, result: &data_data),
        instruction: Bus$(control: insn_control, result: &insn_data),
        load_done: &true,
    );

    let csr_port = inst spadev::csr::mret_csr(clk, rst);

    let CpuResult$(last_write: _, pc, ebreak: _, trace) = inst(5) cpu(clk, rst, buses, csr_port, PROGRAM_BASE);

    let data_control_val = inst read_mut_wire(data_control);
    let insn_control_val = inst read_mut_wire(insn_control);
    let data_is_write = match data_control_val.cmd {
        Command::Write(_) => true,
        _ => false,
    };
    let data_write_value = match data_control_val.cmd {
        Command::Write(val) => val,
        _ => 0,
    };
    Out$(
        insn_control: insn_control_val,
        data_control: data_control_val,
        data_is_write,
        data_write_value,
        pc,
        trace
    )
}
