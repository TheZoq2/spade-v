# Runner for embench-iot benchmarks

The benchmarks are annoying to build since gcc cross compilation is not super
fun. The best option I've found is nix to build the benchmarks. Run `nix-shell` to enter
an interactive environment where they can be built.

The nix stuff is based on https://github.com/sergeiandreyev/riscv-nix but with
everything except the riscv toolchain stripped out.

To run it, run `nix-shell shell.nix` then in the `embench-iot` directory, run
```bash
./build_all.py --arch riscv32 --chip generic --board spade-v --cc riscv32-none-elf-gcc --cflags="" --ldflags="-Wl,-gc-sections" --user-libs="-lm"
```

From this, the normal `swim test` flow can be run
