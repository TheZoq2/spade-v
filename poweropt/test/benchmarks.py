# top=main::top

import sys
import os
from pathlib import Path

extra_path = str(Path(os.environ["SWIM_ROOT"]) / ".." / "extra_python")
sys.path.append(extra_path)
from elf_loading import asm_to_hex,elf_instructions


from cocotb.handle import SimHandleBase

from cocotb import triggers
from cocotb.clock import Clock
from cocotb.regression import TestFactory, Tuple

import os
import glob

from typing import Any, Dict, List

from spade import *


async def update_call_stack(s: SpadeExt, clk):
    return
    trace = s.o.trace;
    while True:
        await FallingEdge(clk)
        if trace.valid == True:
            if trace.op == "spadev::instructions::Op::JAL":
                if trace.rd_idx != 0:
                    dest = int(trace.jump_target.value())
                    print(f"Call to 0x{dest:x} link reg: x{trace.rd_idx.value()}")
            if trace.op == "spadev::instructions::Op::JALR":
                # Check if this is a ret
                if trace.rd_idx == 0 and trace.rs1_idx == 1 and trace.i_imm == 0:
                    dest = int(trace.jump_target.value())
                    print(f"Return")

# Resets and initializes the processor and returns when it is ready to
# run the first instruction
async def setup_test(dut: SimHandleBase):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    await triggers.ClockCycles(clk, 10)
    # Wait another half cycle for everything to align
    await FallingEdge(clk)
    s.i.rst = "false"

    return s

async def test_program(dut, code: str, runtime: int, expected: str):
    insns = asm_to_hex(code)

    clk = dut.clk_i

    s = await setup_test(dut, insns)

    await triggers.ClockCycles(clk, runtime)

    s.o.last_write.assert_eq(expected)

    return s

class Triggers:
    start_trigger: int
    stop_trigger: int
    warm_caches: int
    benchmark_body: int

    def __init__(self, symbols: Dict[str, Any]):
        self.start_trigger = symbols["start_trigger"]["st_value"]
        self.stop_trigger = symbols["stop_trigger"]["st_value"]
        self.warm_caches =  symbols["warm_caches"]["st_value"]
        self.benchmark_body =  symbols["benchmark_body"]["st_value"]

async def drive_insn_bus(
    s: SpadeExt, 
    clk, 
    instructions: List[Tuple[int, str]],
    triggers: Triggers
):
    instructions_map = {addr: insn for (addr, insn) in instructions}
    while True:
        if s.o.insn_control.cmd == "spadev::bus::Command::Read":
            addr = int(s.o.insn_control.addr.value())

            if s.o.trace.valid == True:
                if addr == triggers.start_trigger:
                    print("Starting benchmark")
                if addr == triggers.stop_trigger:
                    print("Stopping benchmark")
                if addr == triggers.warm_caches:
                    print("Warming caches")
                if addr == triggers.benchmark_body:
                    print("Entering benchmark body")

            await FallingEdge(clk)
            s.i.insn_data = f"0x{instructions_map[addr]}"
        else:
            await FallingEdge(clk)

async def drive_data_bus(
    s: SpadeExt,
    clk,
    instructions: List[Tuple[int, str]],
):
    values = {}
    instructions_map = {addr: insn for (addr, insn) in instructions}
    while True:
        is_read = s.o.data_control.cmd == "spadev::bus::Command::Read"
        addr = int(s.o.data_control.addr.value())

        if s.o.data_is_write == True:
            # print(f"Writing {s.o.data_write_value.value()} to {addr:x} around {int(s.o.pc.value()):x}");
            val = int(s.o.data_write_value.value())
            values[addr] = val
            # if addr == 0xffdc:
            # print(f"Writing {val:x} to {addr:x} around {int(s.o.pc.value()):x}")

        await FallingEdge(clk)
        if is_read:
            # print(f"Reading from {addr:x} around {int(s.o.pc.value()):x}");
            if addr in instructions_map:
                s.i.data_data = f"0x{instructions_map[addr]}"
            elif addr in values:
                # print(f"Responding to {addr:x} with {values[addr]:x}")
                s.i.data_data = values[addr]
            else:
                raise Exception(f"{addr:x} read before being written around {int(s.o.pc.value()):x}")


async def isa_test(dut, elf_file):
    print(f"Running {elf_file}")
    program = elf_instructions(elf_file)

    clk = dut.clk_i

    s = await setup_test(dut)

    insn_worker = await cocotb.start(drive_insn_bus(
            s,
            clk,
            program.instructions,
            Triggers(program.symbols)
        ))
    data_worker = await cocotb.start(drive_data_bus(s, clk, program.instructions))
    call_stack_worker = await cocotb.start(update_call_stack(s, clk))

    try:
        CYCLES_PER_CHECK = 100
        # Run n clock cycles a time, waiting for an ebreak signal.
        # If we run 10k cycles and still don't trigger ebreak, timeout
        cycles = 0
        while cycles < 100_000:
            await triggers.ClockCycles(dut.clk_i, CYCLES_PER_CHECK, rising=False)
            cycles += CYCLES_PER_CHECK

        # We never found ebreak, so this is an error
        # But we should sanity check this to be sure
        raise Exception("Test timeout")
    finally:
        insn_worker.cancel()
        data_worker.cancel()
        call_stack_worker.cancel()



test_files = list(filter(
     lambda f: not f.endswith(".o"), 
     glob.glob(os.environ["SWIM_ROOT"] + "/embench-iot/bd/src/**/*")
 ))
test_files.sort()

tf = TestFactory(test_function = isa_test)
tf.add_option(
    "elf_file",
    filter(lambda x: "crc32" in x, test_files)
)
tf.generate_tests()
