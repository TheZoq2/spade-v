lui x1, 1
auipc x1, 1
jal x1, 2
jalr x1, 2
beq x1, x2, 2
bne x1, x2, 2
blt x1, x2, 2
bge x1, x2, 2
bltu x1, x2, 2
bgeu x1, x2, 2
lb x1,1(x2)
lh x1,1(x2)
lw x1,1(x2)
lbu x1,1(x2)
lhu x1,1(x2)
sb x1,1(x2)
sh x1,1(x2)
sw x1,1(x2)
addi x1, x2, 1
slti x1, x2, 1
sltiu x1, x2, 1
xori x1, x2, 1
ori x1, x2, 1
andi x1, x2, 1
slli x1, x2, 1
srli x1, x2, 1
srai x1, x2, 1
add x1, x2, x3
sub x1, x2, x3
sll x1, x2, x3
slt x1, x2, x3
sltu x1, x2, x3
xor x1, x2, x3
srl x1, x2, x3
sra x1, x2, x3
or x1, x2, x3
and x1, x2, x3
// fence
// fence.tso
// pause
// ecall
// ebreak
// invalid
