#     li t1,1
#     li t2,2048
#     nop
#     nop
#     sb t1,0(t2)
# hold:
#     j hold
#     nop
#     nop
#     nop

.equ ONE_SECOND, 25000000

# Memory mappings
.equ LED_BASE, 0x0100_0000

.equ TIMER_BASE, 0x0200_0000
.equ TIMER_WRAPAROUND_TIME, 0
.equ TIMER_FLAG, 8

.equ STACK_TOP, 128

    # Setup stack pointer
    li sp, STACK_TOP
    li t0, ONE_SECOND

    jal x1,setup_timer

    li t1, 0 # LED state
loop:
    li t2, TIMER_BASE
    lw t0,TIMER_FLAG(t2)

    beq zero, t0, notoggle # If the flag is 0, no wraparound has occurred
    # Otherwise toggle the led
    jal x1,toggle_led
notoggle:
    j loop



toggle_led:
    sw x1,0(sp)
    sw t2,-4(sp)
    addi sp,sp,-4

    li t2, LED_BASE
    # Update LED
    beq zero,t1,was_off
    li t1, 0
    j led_write
was_off:
    li t1, 1
led_write:
    # Write to LED periphreal
    # li t1, 2
    nop
    nop
    sb t1,0(t2)

    addi sp,sp,4
    lw t2,-4(sp)
    lw x1,0(sp)

    jalr x0,0(x1)


# Memory mapping of timer peripheral
setup_timer:
    # Stack push
    sw x1,0(sp)
    sw t0,-4(sp)
    sw t1,-8(sp)

    li t0,TIMER_BASE
    li t1,ONE_SECOND
    sw t1,TIMER_WRAPAROUND_TIME(t0)

    # Stack pop
    lw t1,-8(sp)
    lw t0,-4(sp)
    lw x1,0(sp)

    jalr x0,0(x1)
