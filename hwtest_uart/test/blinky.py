# top=led_blink::top_blink

from spade import *
import cocotb
from cocotb.clock import Clock
import cocotb.triggers as triggers

@cocotb.test()
async def full(dut):
    s = SpadeExt(dut)

    clk = dut.clk_25mhz
    await cocotb.start(Clock(clk, 1, units = 'ns').start())
    
    s.i.ftdi_txd = "false"

    await triggers.ClockCycles(clk, 10_000)

    return s
