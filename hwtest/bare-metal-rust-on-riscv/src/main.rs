#![no_std]
#![no_main]

use core::{arch::global_asm, panic::PanicInfo};

global_asm!(include_str!("init.s"));


/// A panic handler is required in Rust, this is probably the most basic one possible
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

const LED_BASE: *mut u32 = 4096 as *mut u32;

fn set_led(on: bool) {
    let val = if on {1} else {0};

    unsafe {
        LED_BASE.write_volatile(val)
    }
}

const TIMER_BASE: *mut u32 = 5120 as *mut u32;
// Offsets in sizeof(u32)
const OFFSET_WRAP_TIME: isize = 0;
const OFFSET_TIMER: isize = 1;
const OFFSET_FLAG: isize = 2;

fn init_timer(cycles: u32) {
    unsafe {
        TIMER_BASE.offset(OFFSET_WRAP_TIME).write_volatile(cycles)
    }
}

fn wraparound() -> bool {
    unsafe {
        TIMER_BASE.offset(OFFSET_FLAG).read_volatile() == 1
    }
}


/// Main program function
#[no_mangle]
extern "C" fn main() -> () {
    // Example: Create a counter peripheral with base address 0x8000_0000
    let mut val = false;
    init_timer(25_000_000);
    loop {
        if wraparound() {
            set_led(val);
            val = !val;
        }
    }
}
