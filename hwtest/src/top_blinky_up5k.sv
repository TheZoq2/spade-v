module top(output pin_1);
    wire clk;
    // Configure internal HS oscillator as 24MHz
    SB_HFOSC #(.CLKHF_DIV("0b11"))  osc(.CLKHFEN(1'b1),     // enable
                                        .CLKHFPU(1'b1), // power up
                                        .CLKHF(clk)         // output to sysclk
                                        ) /* synthesis ROUTE_THROUGH_FABRIC=0 */;
    reg [5:0] reset_cnt = 0;
    wire resetn = &reset_cnt;
    always @(posedge clk) begin
        reset_cnt <= reset_cnt + !resetn;
    end

    \proj::led_blink::top  cpu
        ( .clk_i(clk)
        , .rst_i(!resetn)
        , .output__({pin_1})
        );
endmodule

