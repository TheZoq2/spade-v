module top(input clk_25mhz, output[7:0] led);
    wire clk;
    assign clk = clk_25mhz;

    reg [5:0] reset_cnt = 0;
    wire resetn = &reset_cnt;
    always @(posedge clk) begin
        reset_cnt <= reset_cnt + !resetn;
    end

    e_proj_led_blink_top cpu
        ( .clk_i(clk)
        , .rst_i(!resetn)
        , .output__({led[0]})
        );
endmodule

