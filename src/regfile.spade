use std::mem::clocked_memory;
use std::mem::read_memory;


struct port RegfilePort {
    write: inv &Option<(uint<5>, int<32>)>,
    idxa: inv &uint<5>,
    idxb: inv &uint<5>,
    read: &(int<32>, int<32>)
}

pipeline(1) use_regfile(
    clk: clock,
    regfile: RegfilePort,
    write: Option<(uint<5>, int<32>)>,
    idxa: uint<5>,
    idxb: uint<5>
) -> (int<32>, int<32>) {
        set regfile.write = write;
        set regfile.idxa = idxa;
        set regfile.idxb = idxb;
    reg;
        *regfile.read
}

pipeline(1) regfile_impl<#uint N>(
    clk: clock,
    write: Option<(uint<5>, int<32>)>,
    idxa: uint<5>,
    idxb: uint<5>
) -> (int<32>, int<32>) {
        let (we, dreg, dval) = match write {
            Option::Some((dreg, dval)) => (true, dreg, dval),
            // NOTE: We really need to add a way to specify don't care here
            _ => (false, 0, 0)
        };

        let regs: Memory<int<32>, N> = inst clocked_memory(clk, [(we, trunc(dreg), dval)]);

        let opa = inst read_memory(regs, trunc(idxa));
        let opb = inst read_memory(regs, trunc(idxb));
    reg;
        // RiscV defines the 0th register as always 0. Since we don't support initial values
        // for memories, we'll force it to 0 like this instead.
        (
            if idxa == 0 {0} else if idxa == dreg {dval} else {opa},
            if idxb == 0 {0} else if idxb == dreg {dval} else {opb}
        )
}

entity regfile(clk: clock) -> RegfilePort {
    let (r, w) = port;

    let out = inst(1) regfile_impl::<32>$(
        clk,
        write: *w.write,
        idxa: *w.idxa,
        idxb: *w.idxb,
    );

    set w.read = out;

    r
}

entity small_regfile(clk: clock) -> RegfilePort {
    let (r, w) = port;

    let out = inst(1) regfile_impl::<8>$(
        clk,
        write: *w.write,
        idxa: *w.idxa,
        idxb: *w.idxb,
    );

    set w.read = out;

    r
}
