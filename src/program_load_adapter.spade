use lib::bus::ControlSignals;
use lib::bus::Command;
use lib::bus::AccessWidth;
use lib::bus::ProgramLoadSignals;
use lib::bus::AddrValue;

// Adapts program loader signals to bus control signals, redirecting
// the control signals from the CPU if the program hasn't been loaded yet
fn program_load_adapter(
    control: ControlSignals,
    load_signals: ProgramLoadSignals
) -> ControlSignals {
    assert match load_signals.data {Some(inner) => ((inner.addr & 0b11) == 0), _ => true};

    if load_signals.done {
        control
    }
    else {
        match load_signals.data {
            Some(inner) => {
                ControlSignals$(
                    access_width: AccessWidth::Full(),
                    addr: inner.addr,
                    cmd: Command::Write(inner.value)
                )
            },
            None => {
                ControlSignals$(
                    access_width: AccessWidth::Full(),
                    addr: 0,
                    cmd: Command::Nop()
                )
            }
        }
    }
}
