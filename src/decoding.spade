use std::conv::concat;
use std::conv::sext;
use std::conv::uint_to_int;


fn rd(insn: uint<32>) -> uint<5> {
    trunc(insn >> 7)
}

fn rs1(insn: uint<32>) -> uint<5> {
    trunc(insn >> 15)
}

fn rs2(insn: uint<32>) -> uint<5> {
    trunc(insn >> 20)
}

fn i_type_immediate(insn: uint<32>) -> int<32> {
    let insn = uint_to_int(insn);
    // Ugly bit selection
    // lifeguard spade#131
    let truncated: int<12> = trunc(insn >> 20);
    sext(truncated)
}

fn u_type_immediate(insn: uint<32>) -> int<32> {
    let insn = uint_to_int(insn);
    // NOTE: This should really use unsigned when we have it
    let pattern: int<33> = 0xff_ff_f0_00;
    insn & trunc(pattern)
}

fn j_type_immediate(insn: uint<32>) -> int<32> {
    let insn = uint_to_int(insn);
    // lifeguard spade #131
    let bit0: int<1> = 0;
    let bit10_1: int<10> = trunc(insn >> 21);
    let bit11: int<1> = trunc(insn >> 20);
    let bit19_12: int<8> = trunc(insn >> 12);
    let bit20: int<1> = trunc(insn >> 31);

    sext(bit20 `concat` bit19_12 `concat` bit11 `concat` bit10_1 `concat` bit0)
}

fn b_type_immediate(insn: uint<32>) -> int<32> {
    let insn = uint_to_int(insn);
    // lifeguard spade #131
    let bit0: int<1> = 0;
    let bit4_1: int<4> = trunc(insn >> 8);
    let bit10_5: int<6> = trunc(insn >> 25);
    let bit11: int<1> = trunc(insn >> 7);
    let bit12: int<1> = trunc(insn >> 31);

    sext(bit12 `concat` bit11 `concat` bit10_5 `concat` bit4_1 `concat` bit0)
}

fn s_type_immediate(insn: uint<32>) -> int<32> {
    let insn = uint_to_int(insn);
    let bit4_0: int<5> = trunc(insn >> 7);
    let bit11_5: int<7> = trunc(insn >> 25);

    sext(bit11_5 `concat` bit4_0)
}

struct DecodingTestOut {
    rd: uint<5>,
    rs1: uint<5>,
    rs2: uint<5>,
    i_imm: int<32>,
    u_imm: int<32>,
    j_imm: int<32>,
    b_imm: int<32>,
    s_imm: int<32>,
}

#[no_mangle]
fn decoding_test_harness(insn: uint<32>) -> DecodingTestOut {
    DecodingTestOut$(
        rd: rd(insn),
        rs1: rs1(insn),
        rs2: rs2(insn),
        i_imm: i_type_immediate(insn),
        u_imm: u_type_immediate(insn),
        j_imm: j_type_immediate(insn),
        b_imm: b_type_immediate(insn),
        s_imm: s_type_immediate(insn),
    )
}
