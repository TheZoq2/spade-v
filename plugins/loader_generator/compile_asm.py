import subprocess
import tempfile
import os
import shutil
import argparse

from typing import List

from elftools.elf.elffile import ELFFile

def hex_from_hexdump_line(regex, line: str):
    if m := regex.match(line):
        return m.groups(1)[0]
    else:
        raise Exception(f"'{line}' does not match regex")


def asm_to_hex(asm_file: str) -> List[str]:
    """
    Compiles the specified assembly code into a list of instructions represented
    as hex
    """
    o_file = tempfile.mktemp()
    # o_file = elf_file

    print(o_file)

    subprocess.run(["riscv32-elf-as", asm_file, "-c", "-o", o_file]).check_returncode()
    shutil.copy(o_file, "build/blinky.o")

    program = elf_instructions(o_file)

    return program.instructions

class Program:
    def __init__(self):
        self.offset = 0
        self.instructions = []


def elf_instructions(elf_file: str) -> Program:
    result = Program()

    with open(elf_file, 'rb') as f:
        sections = ELFFile(f).iter_sections()

        found_sections = []
        for section in sections:
            print("type:", section['sh_type'])
            print(section.name);
            if section['sh_type'] == 'SHT_PROGBITS':
                if section.name in [
                    ".eh_frame",
                    ".riscv.attributes",
                    ".debug_abbrev",
                    ".debug_info",
                    ".debug_aranges",
                    ".debug_ranges",
                    ".debug_str",
                    ".debug_pubnames",
                    ".debug_pubtypes",
                    ".debug_line",
                    ".comment",
                    ".symtab",
                    ".shstrtab",
                    ".strtab",
                ]:
                    continue
                if section.name == ".tohost":
                    continue

                # found_sections.append(section.name)
                result.offset = section['sh_addr'] 

                chunk = []
                for byte in section.data():
                    chunk += [byte];
                    if len(chunk) == 4:
                        new_hex = ""
                        for b in chunk[::-1]:
                            new_hex += f"{b:02x}"
                        chunk = []
                        result.instructions.append(new_hex)

    for (i, insn) in enumerate(result.instructions):
        if i*4 >= 4096:
            raise ValueError("Program does not fit")

        print(f"{0x8000_0000 + i*4:x}", insn)

    return result

def main(source_file, template_file, target_file):
    if os.path.exists(target_file):
        dependent_files = [template_file, source_file, __file__]
        target_time = os.path.getmtime(target_file)
        needs_rebuild = any(map(
            lambda file: not os.path.exists(file) or os.path.getmtime(file) > target_time,
            dependent_files)
        )
    else:
        needs_rebuild = True

    if not needs_rebuild:
        return

    hex = asm_to_hex(source_file)

    program_string = '\n        '.join(list(map(lambda h: f"0x{h},", hex)))
    # Add the end-of-program marker, otherwise the synthesis tool will optimize
    # away the whole CPU :P
    program_string += "\n        0x0"

    with open(template_file) as f:
        template = f.read()

    new_loader = template.replace("$PROGRAM$", program_string)

    os.makedirs("src/programs", exist_ok=True)

    with open(target_file, "w") as f:
        f.write(new_loader)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compile asm code and embed it in aspade module')
    parser.add_argument('elf', type=str)
    parser.add_argument('template', type=str)
    parser.add_argument('target', type=str)

    args = parser.parse_args()
    main(source_file = args.elf, template_file = args.template, target_file = args.target)
