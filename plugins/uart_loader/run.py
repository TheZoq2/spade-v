import sys
import os
from pathlib import Path
from typing import List
import serial
import serial.tools.list_ports

plugin_dir = sys.argv[1]

extra_path = str(Path(plugin_dir) / ".." / ".."/ "extra_python")
print(extra_path)
sys.path.append(extra_path)
from elf_loading import elf_instructions

HEADER_DONE = 0;
HEADER_OFFSET = 1;
HEADER_DATA = 2;
HEADER_INVALID = 0x7F;

def write_command(serial_port: serial.Serial, header: int, data: bytes):
    serial_port.write(header.to_bytes(1))
    serial_port.write(data)

def main():
    asm_file = sys.argv[2]

    if len(sys.argv) != 4:
        print("Specify a serial port. Available ports")
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))
        return

    with serial.Serial(sys.argv[3], 115200) as serial_port:
        program = elf_instructions(asm_file)

        for (addr, value) in program.instructions:
            # We'll be lazy and just write the address every time
            write_command(serial_port, HEADER_OFFSET, addr.to_bytes(4, byteorder='big'))
            write_command(serial_port, HEADER_DATA, int(value, 16).to_bytes(4, byteorder='big'))

        write_command(serial_port, HEADER_DONE, (0).to_bytes(4, byteorder='big'))



if __name__ == "__main__":
    main()
