# top=peripherals::timer::timer_test_harness

from spade import *

from cocotb import triggers
from cocotb.triggers import FallingEdge
from cocotb.clock import Clock

@cocotb.test()
async def timer_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.memory_command = "Command::Nop()"
    await FallingEdge(clk)
    s.i.rst = "true"
    await FallingEdge(clk)
    s.i.rst = "false"

    s.i.mem_range = "(1024, 2048)"
    s.i.addr = "1024"
    s.i.memory_command = "Command::Write(10)"
    await FallingEdge(clk)

    # Read the timer
    s.i.addr = "1028"
    s.i.memory_command = "Command::Read()"
    for i in range(0, 11):
        await FallingEdge(clk)
        s.o.assert_eq(f"{i}")

    await FallingEdge(clk)
    s.o.assert_eq("0")

    # Flag should now be set. Read it to find out. A read without re should
    # not clear it
    s.i.addr = "1032"
    s.i.memory_command = "Command::Nop()"
    await FallingEdge(clk)
    # s.o.assert_eq("1")

    # Proper read
    s.i.memory_command = "Command::Read()"
    await FallingEdge(clk)
    s.o.assert_eq("1")

    # Should clear
    await FallingEdge(clk)
    s.o.assert_eq("0")






