# top = uart_loader::test_harness

from typing import List
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from spade import *

import cocotb

HEADER_DONE = 0;
HEADER_OFFSET = 1;
HEADER_DATA = 2;
HEADER_INVALID = 0x7F;

async def stream_packet(clk, s: SpadeExt, bytes: List[int]):
    for byte in bytes:
        s.i.uart_stream = f"UartOut::Ok({byte})"
        await FallingEdge(clk)
    s.i.uart_stream = f"UartOut::None()"

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 2).start())

    s.i.uart_stream = "UartOut::None()"
    s.i.rst = "true"
    await FallingEdge(clk)
    s.i.rst = "false"

    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")

    # Loading bytes works
    await stream_packet(clk, s, [HEADER_DATA, 0x01, 0x02, 0x03, 0x04])
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: Some(AddrValue$(addr: 0, value: 0x01020304)))")
    await FallingEdge(clk)
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")

    # Changing the address works
    await stream_packet(clk, s, [HEADER_OFFSET, 0x80, 0x00, 0x00, 0x10])
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")
    await stream_packet(clk, s, [HEADER_DATA, 0x01, 0x02, 0x03, 0x04])
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: Some(AddrValue$(addr: 0x8000_0010, value: 0x01020304)))")
    await FallingEdge(clk)
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")

    # Address is incremented
    await stream_packet(clk, s, [HEADER_DATA, 0x02, 0x03, 0x04, 0x05])
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: Some(AddrValue$(addr: 0x8000_0014, value: 0x02030405)))")
    await FallingEdge(clk)
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")

    # Done packet works
    await stream_packet(clk, s, [HEADER_DONE, 0x02, 0x03, 0x04, 0x05])
    s.o.assert_eq("ProgramLoadSignals$(done: false, data: None())")
    await FallingEdge(clk);
    s.o.assert_eq("ProgramLoadSignals$(done: true, data: None())")


    # More data after done is ignored
    await stream_packet(clk, s, [HEADER_DATA, 0x02, 0x03, 0x04, 0x05])
    s.o.assert_eq("ProgramLoadSignals$(done: true, data: None())")
