# top=cpu::cpu_test_harness

import sys
import os
from pathlib import Path

extra_path = str(Path(os.environ["SWIM_ROOT"]) / "extra_python")
sys.path.append(extra_path)
from elf_loading import asm_to_hex,elf_instructions


from cocotb.handle import SimHandleBase

from cocotb import triggers
from cocotb.triggers import FallingEdge
from cocotb.clock import Clock
from cocotb.regression import TestFactory, Tuple

import os
import glob

from typing import List

from spade import *


# Resets and initializes the processor and returns when it is ready to
# run the first instruction
async def setup_test(dut: SimHandleBase, insns: List[Tuple[int, str]]):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.load_port = "ProgramLoadSignals$(done: false, data: None())"
    s.i.rst = "true"
    await triggers.ClockCycles(clk, 10)
    # Wait another half cycle for everything to align
    await FallingEdge(clk)
    s.i.rst = "false"

    for addr, insn in insns:
        # TODO: Put the data where it belongs in memory
        s.i.load_port = f"ProgramLoadSignals$(done: false, data: Some(spadev::bus::AddrValue$(addr: {addr}, value: 0x{insn})))"
        await FallingEdge(clk)
    s.i.load_port = "ProgramLoadSignals$(done: true, data: None())"

    return s

async def test_program(dut, code: str, runtime: int, expected: str):
    insns = asm_to_hex(code)

    clk = dut.clk_i

    s = await setup_test(dut, insns)

    await triggers.ClockCycles(clk, runtime)

    s.o.last_write.assert_eq(expected)

    return s


@cocotb.test()
async def register_fowrarding(dut):
    program = """
        li	ra,0
        addi	ra,ra,1
        addi	ra,ra,1
        addi	ra,ra,1
    _end_loop:
        j _end_loop
        """
    await test_program(dut, program, 20, "Some(3)")



@cocotb.test()
async def forward_jumps_to_correct_location(dut):
    program = """
        li	ra,0
        nop
        nop
        j done
        addi ra,ra,1
        nop
        nop
        nop
    done:
        addi	ra,ra,2
    _end_loop:
        j _end_loop
        """
    await test_program(dut, program, 20, "Some(2)")


@cocotb.test()
async def jal_updates_the_link_register(dut):
    program = """
        li ra,0
        jal ra,done
    done:
        nop
    _end_loop:
        j _end_loop
        """
    await test_program(dut, program, 20, "Some(0x8000_0008)")


@cocotb.test()
async def jal_runs_following_after_jump(dut):
    program = """
        li t0,0
        jal x1,sub
        addi t0,t0,1
        j loop
    loop:

        nop
    sub:
        li t0,10
        jalr x0,0(x1)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(11)")

 
@cocotb.test()
async def beq_does_not_take_jump_if_non_eq_operands(dut):
    program = """
        li sp,0
        li ra,1
        beq ra,sp,skip
        addi ra,ra,1
    skip:
        mv ra,ra
    _end_loop:
        j _end_loop
        """
    await test_program(dut, program, 20, "Some(2)")


@cocotb.test()
async def beq_takes_jump_for_eq_operands(dut):
    program = """
        li sp, 1
        li ra, 1
        beq ra, sp, skip
        addi ra, ra, 1
        nop
        nop
    skip:
        mv ra,ra
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(1)")


@cocotb.test()
async def bne_takes_jump_for_eq_operands(dut):
    program = """
        li sp,0
        li ra,1
        bne ra,sp,skip
        addi ra,ra,1
    skip:
        mv ra,ra
    _end_loop:
        j _end_loop
        """
    await test_program(dut, program, 20, "Some(1)")


@cocotb.test()
async def bne_does_not_take_jump_if_non_eq_operands(dut):
    program = """
        li sp, 1
        li ra, 1
        bne ra, sp, skip
        addi ra, ra, 1
        nop
        nop
    skip:
        mv ra,ra
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(2)")

@cocotb.test()
async def bgeu_takes_branch_if_0(dut):
    program = """
        li sp, 0
        li ra, 0
        bgeu ra, sp, skip
        addi ra, ra, 1
        nop
        nop
    skip:
        mv ra,ra
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(0)")


@cocotb.test()
async def bgeu_works_on_0_max_value(dut):
    program = """
        li t0, 0
        li ra, 0xffffffff
        li sp, 0
        bgeu ra, sp, skip
        addi t0, t0, 1
        nop
        nop
    skip:
        mv t0,t0
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(0)")

@cocotb.test()
async def sb_and_lw_work(dut):
    program = """
        li t0, 0
        li t1, 1
        li t2, 2
        li t3, 3
        li t4, 4
        sb t1, 0(t0)
        sb t2, 1(t0)
        sb t3, 2(t0)
        sb t4, 3(t0)
        lw t5, 0(t0)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(0x04030201)")

@cocotb.test()
async def sh_and_lw_work(dut):
    program = """
        li t0, 0
        li t1, 1
        li t2, 2
        li t3, 3
        li t4, 4
        sh t1,0(t0)
        sh t3,2(t0)
        lw t5, 0(t0)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(0x00030001)")

@cocotb.test()
async def reg_forwarding_works_for_memory_ops(dut):
    program = """
        li t0, 0
        li t1, 10
        sw t1, 0(t0)
        lw t2, 0(t0)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(10)")


@cocotb.test()
async def sw_with_negative_offset_works(dut):
    program = """
        li t0, 8
        li t2, 4
        li t1, 10
        sw t1, -4(t0)
        lw t2, 0(t2)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(10)")

@cocotb.test()
async def loading_large_constants_works(dut):
    program = """
        lui t0,0x2dc
        addi t0,t0,1728
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(3000000)")


@cocotb.test()
async def jalr_works(dut):
    program = """
    _start:
        li t0,0
        lui x1,%hi(target)
        addi x1,x1,%lo(target)
        jalr sp,x1,4
    target:
        addi t0,t0,1
        addi t0,t0,2
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(2)")

@cocotb.test()
async def jalr_sets_the_destination_register(dut):
    program = """
    _start:
        li t0,0
        lui x1,%hi(target)
        addi x1,x1,%lo(target)
        jalr sp,x1,4
    target:
        nop
        nop
        nop
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 20, "Some(0x8000_0010)")


@cocotb.test()
async def immediate_use_of_load_result_works(dut):
    program = """
    _start:
        li t4,0
        li t0,12
        li t1,20
        sw t1,0(t0)
        li t3,0
        lw t1,0(t0)
        add t4,t4,t1
        addi t4,t4,1
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 100, "Some(21)")

@cocotb.test()
async def unaligned_word_load_store_works(dut):
    program = """
    _start:
        li t4,0
        li t0,11
        li t1,20
        sw t1,0(t0)
        lw t1,0(t0)
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 100, "Some(20)")



@cocotb.test()
async def calls_and_returns_work(dut):
    program = """
    _start:
        li sp,100
        li t0,0 # Result storage
        li t2,10 # Smashed and restored by subroutine

        jal x1,subroutine
        add t0,t0,t2
    loop:
        j loop
        addi t0,t0,28

    subroutine:
        sw x1,0(sp)
        sw t2,4(sp)
        li t2,3
        add t0,t0,t2
        lw t2,4(sp)
        lw x1,0(sp)
        jalr x0,0(x1)

    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 30, "Some(13)")


@cocotb.test()
async def csr_mepc_read_and_write_works(dut):
    program = """
        li t0,0xabc
        csrw mepc,t0
        csrr t1,mepc
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 30, "Some(0xabc)")

@cocotb.test()
async def csr_mepc_read_is_forwarded(dut):
    program = """
        li t0,1234
        csrw mepc,t0
        csrr t1,mepc
        addi t1,t1,1
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 30, "Some(1235)")



@cocotb.test()
async def mret_works(dut):
    program = """
        li t1,0
        lui t0,%hi(target)
        addi t0,t0,%lo(target)
        csrw mepc,t0
        mret
        addi t1,t1,1 # Should be skipped
    target:
        addi t1,t1,2 # This is the next instruction to be executed
        addi t1,t1,3 # So should this
    _end_loop:
        j _end_loop
        """

    await test_program(dut, program, 30, "Some(5)")


@cocotb.test()
async def ebreak_works(dut):
    program = """
        li t1,5 # Last visible load
        ebreak
        li t1,10
    _end_loop:
        j _end_loop
        """

    s = await test_program(dut, program, 30, "Some(5)")

    s.o.ebreak.assert_eq("true")


@cocotb.test()
async def ecall_semi_works(dut):
    program = """
        li t1,5 # Last visible load
        ecall
        li t1,10
    _end_loop:
        j _end_loop
        """

    s = await test_program(dut, program, 30, "Some(5)")

    s.o.ebreak.assert_eq("true")

@cocotb.test()
async def auipc_works(dut):
    program = """
        nop # 0
        nop # 4
        auipc t0,0x01 # 8
    _end_loop:
        j _end_loop
        """

    s = await test_program(dut, program, 30, "Some(0x8000_1008)")



# @cocotb.test()
# async def __elf_test(dut):
#     print(os.getcwd())
#     elf = "../../isa_test/riscv-tests/isa/rv32ui-p-addi"
#     elf_instructions(elf)

async def isa_test(dut, elf_file):
    print(f"Running {elf_file}")
    program = elf_instructions(elf_file)

    s = await setup_test(dut, program.instructions)

    CYCLES_PER_CHECK = 100
    # Run n clock cycles a time, waiting for an ebreak signal.
    # If we run 10k cycles and still don't trigger ebreak, timeout
    cycles = 0
    while cycles < 10_000:
        await triggers.ClockCycles(dut.clk_i, CYCLES_PER_CHECK, rising=False)
        cycles += CYCLES_PER_CHECK

        if s.o.ebreak.value() == "true":
            s.o.last_write.assert_eq("Some(0)")
            return

    # We never found ebreak, so this is an error
    # But we should sanity check this to be sure
    s.o.ebreak.assert_eq("false")
    raise Exception("Test timeout")



test_files = list(filter(lambda f: not f.endswith(".dump"), glob.glob(os.environ["SWIM_ROOT"] + "/isa_test/riscv-tests/isa/rv32ui-p-*")))
test_files.sort()

tf = TestFactory(test_function = isa_test)
tf.add_option(
    "elf_file",
    test_files
)
tf.generate_tests()
