
// #![no_std]
// #![no_main]
// 
// use core::arch::global_asm;
// 
// extern crate panic_halt;
// 
// global_asm!(include_str!("init.s"));
// 
// const LED_BASE: *mut usize = 4096 as *mut usize;
// const TIMER_BASE: *mut usize = 5120 as *mut usize;
// 
// fn set_led(on: bool) {
//     let val = if on {1} else {0};
// 
//     unsafe {
//         *LED_BASE = val;
//     }
// }
// 
// #[no_mangle]
// extern "C" fn main() -> ! {
//     let mut val = true;
//     loop {
//         set_led(val);
//         val = !val;
//     }
// }
