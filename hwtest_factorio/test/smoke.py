#top = led_blink::top_blink

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.rst = "true"
    for _ in range(0, 10):
        await FallingEdge(clk)
    s.i.rst = "false"

    for i in range(0, 1000):
        await FallingEdge(clk)
