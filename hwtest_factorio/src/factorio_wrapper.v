`default_nettype none

module top_blink_factorio (
    (* clock *)
    input clk,
    (* signal = "virtual/signal-red" *)
    input rst,

    (* signal = "virtual/signal-0" *)
    output led,
    (* signal = "virtual/signal-1" *)
    output pc,
);
    wire dummy;
    \top_blink main(.clk(clk), .rst(rst), .led_out(led), .pc_out(pc), .output__(dummy));
endmodule
