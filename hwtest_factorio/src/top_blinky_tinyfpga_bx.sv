module top(input clk, output led);
    reg [5:0] reset_cnt = 0;
    wire resetn = &reset_cnt;
    always @(posedge clk) begin
        reset_cnt <= reset_cnt + !resetn;
    end

    wire dummy;

    \proj::led_blink::top_blink  cpu
        ( .clk_i(clk)
        , .rst_i(!resetn)
        , .output__({led, dummy})
        );
endmodule

