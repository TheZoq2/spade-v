module top(input clk_25mhz, input  [6:0] btn, output[7:0] led);
    wire clk;
    assign clk = clk_25mhz;

    reg [5:0] reset_cnt = 0;
    wire resetn = &reset_cnt;
    always @(posedge clk) begin
        reset_cnt <= reset_cnt + !resetn;
    end

    wire debug_led;
    wire out_led;
    wire load_done;
    assign led[0] = out_led;
    assign led[1] = 0;
    assign led[2] = 0;
    assign led[3] = 0;
    assign led[4] = 0;
    assign led[5] = 0;
    assign led[6] = 0;
    assign led[7] = 0;

    \top_blink  cpu
        ( .clk_i(clk)
        , .rst_i(btn[1])
        , .output__({out_led, load_done})
        );
endmodule

